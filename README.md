# Behapi demo

This project is a demo for [Behapi](https://github.com/Taluu/Behapi),
a [Behat](http://behat.org) extension to test HTTP API.

## Install

```
composer install --dev
```

### Test

First you need to start the test server

```
php server.php
```

Then you can run the tests

```
./vendor/bin/behat
```
