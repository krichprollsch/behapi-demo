@http
Feature: Echo me

Scenario: echo my request
    When I create a "POST" request to "/echo"
    And I set the following body:
    """
    {
      "foo": {
        "bar": "baz"
      }
    }
    """
    And I send the request
    Then the status code should be 200
    And the content-type should be equal to "application/json"
    And the response should be a valid json response
    And in the json, "foo.bar" should be equal to "baz"

