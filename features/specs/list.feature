@http
Feature: List blog posts

Scenario: list all blog posts
    When I create a "GET" request to "/posts"
    And I send the request
    Then the status code should be 200
    And the content-type should be equal to "application/json"
    And the response should be a valid json response
    And in the json, "posts" should have "2" elements
    And in the json, "posts[0].id" should be equal to "0"
    And in the json, "posts[0].title" should contain "post"
