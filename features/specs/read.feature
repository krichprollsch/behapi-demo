@http
Feature: Read a blog post

Scenario: Read an existing blog post
    When I create a "GET" request to "/posts/0"
    And I send the request
    Then the status code should be 200
    And the content-type should be equal to "application/json"
    And the response should be a valid json response
    And in the json, "id" should be equal to "0"
    And in the json, "title" should contain "post"

Scenario: Read an nonexistent
    When I create a "GET" request to "/posts/3"
    And I send the request
    Then the status code should be 404
    And the content-type should be equal to "application/json"
