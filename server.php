<?php
require __DIR__.'/vendor/autoload.php';

use Rxnet\Httpd\Httpd;
use Rxnet\Httpd\HttpdRequest;
use Rxnet\Httpd\HttpdResponse;

$posts = [
    ['id' => 0, 'title' => 'first post'],
    ['id' => 1, 'title' => 'second post'],
];

$httpd = new Httpd();

// posts
$httpd->route('GET', '/posts', function (HttpdRequest $request, HttpdResponse $response) use ($posts) {
    $response->json(['posts' => $posts]);
});
$httpd->route('GET', '/posts/{id}', function (HttpdRequest $request, HttpdResponse $response) use ($posts) {
    $id = $request->getRouteParam('id');
    $response->json(
        $posts[$id] ?? null,
        isset($posts[$id]) ? 200 : 404
    );
});

// echo
$httpd->route('POST', '/echo', function (HttpdRequest $request, HttpdResponse $response) {
    try {
        $response->json($request->getJson());
    } catch (\Exception $e) {
        $response->json(['err' => $e->getMessage()], 500);
    }
});

$httpd->listen(2080);
